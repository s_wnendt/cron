<?php
require_once(__DIR__ . '/vendor/autoload.php');
//require_once(__DIR__ . '/../../../vendor/autoload.php');
include_once('PhpCsvValidator.php');
include_once('SFTPConnection.php');
//require_once $_SERVER['HOME'] . '/' . $_SERVER['SERVER_NAME'] . '/vendor/autoload.php';

use Zend\Config\Config as Config;
use PhpCsvValidator as CsvValidate;
use Symfony\Component\Dotenv\Dotenv;

/**
 * call the function from cli
 */
$view         = new cronjobs();
$functionName = $argv[1];
$param        = $argv[2];
//for example: php Cronjobs.php getExport idealo
$view->$functionName($param);


class Cronjobs
{

    /**
     * @var Zend\Log\
     */
    private $logger;

    /**
     * @var Zend\Config\Config
     */
    private $config;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $dotenv = new Dotenv();
        $dotenv->load(__DIR__ . '/../../.env');
        $this->config    = $this->getConfigForEnv();;
        $this->logger    = new Zend\Log\Logger();
        $stream          = $this->getStream();
        $writer          = new Zend\Log\Writer\Stream($stream);
        $this->logger->addWriter($writer);
    }


    /**
     * get the export from shopware and copy this to the FTP Server
     *
     *
     * @param string $exportName
     * @throws Exception
     */
    public function getExport(string $exportName)
    {
        $this->logger->notice($exportName . ' is started at ' . date("d.m.Y - H:i:s", time()));
        $exportConfig = $this->config->get($exportName);

        if ($exportConfig) {
            // use the existing GoogleDE Export File and copy to the SFTP
            if ($exportName === 'bingXML' || $exportName === 'googleDE2XML') {
                $this->copyFilesForExport($exportName, $exportConfig);
                exit;
            }

            $exportUrl = $exportConfig->url;
            $localFile = $exportConfig->localFile;
            $ftpFolder = $exportConfig->ftp->folder;

            echo '--- ' . $exportName . ' export was running ---' . PHP_EOL;

            $exportFile  = $this->curlDownload($exportUrl, $localFile, $exportName);

            if ($exportFile !== '') {
                // check if the file is valid and only then upload it to the FTP servers
                $isValidFile = $this->checkExportFile($exportFile);

                if ($isValidFile) {
                    echo '---' . $exportName . ' export was successfully created ---' . PHP_EOL;
                    $this->logger->notice($exportName . ' Export was successfully created....');
                    $ftpConfig = $exportConfig->ftp;

                    // BING/Google is SFTP Server
                    if ($exportConfig->isSFTP) {
                        try {
                            $sftp = new SFTPConnection($ftpConfig->server, $ftpConfig->port);
                            $sftp->login($ftpConfig->user, $ftpConfig->pw);
                            $sftp->uploadFile($localFile, $exportConfig->filename);
                            $this->logger->err('SFTP Connection and uploading success');
                        } catch (Exception $e) {
                            echo $e->getMessage() . "\n";
                        }
                    } else {
                        $copyToFtp = $this->copyToFtpServer($ftpConfig, $ftpFolder, $localFile, $exportConfig->filename);
                        if (null === $copyToFtp) {
                            $this->logger->err('FTP Connection not success');
                        }
                    }
                } else {
                    $this->logger->err('ExportFile ' . $exportName . ' not successfully created ');
                }
            }
        } else {
            $this->logger->err('Key: ' . $exportName . ' does not exists as export configuration');
        }
    }

    /**
     * @param string $exportName
     * @param $exportConfig
     * @return void
     */
    private function copyFilesForExport (string $exportName, $exportConfig)
    {
        $ftpConfig = $exportConfig->ftp;
        $localFile = $exportConfig->localFile;
        $isValidFile = $this->checkExportFile($localFile);

        if ($isValidFile && $localFile !== '' && $exportConfig->isSFTP) {
            try {
                $sftp = new SFTPConnection($ftpConfig->server, $ftpConfig->port);
                $sftp->login($ftpConfig->user, $ftpConfig->pw);
                $sftp->uploadFile($localFile, $exportConfig->filename);
                $this->logger->err('SFTP Connection and uploading success');
                exit;
            } catch (Exception $e) {
                echo $e->getMessage() . "\n";
            }
        }
    }

    /**
     * @param string $url
     * @param string $file
     * @param string $exportName
     * @return bool
     *
     */
    private function curlDownload(string $url, string $file, string $exportName)
    {
        $dateStart = date("d.m.Y - H:i:s", time());

        if (!$this->checkCurlBasicFunctions()) {
            return false;
        } else {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
            // required for HTTP error codes to be reported
            curl_setopt($ch, CURLOPT_FAILONERROR, true);
            if ($ch && $url !== '') {
                $fp = fopen($file, 'w');
                if ($fp) {
                    if (!curl_setopt($ch, CURLOPT_URL, $url)) {
                        fclose($fp); // to match fopen
                        curl_close($ch); // to match curl_init
                        $this->logger->err('CURL Error: fail curl_setopt(CURLOPT_URL)');
                        return false;
                    }
                    if (!curl_setopt($ch, CURLOPT_FILE, $fp)) {
                        $this->logger->err('CURL Error: curl_setopt(CURLOPT_FILE)');
                        return false;
                    }
                    if (!curl_setopt($ch, CURLOPT_HEADER, 0)) {
                        $this->logger->err('CURL Error: fail curl_setopt(CURLOPT_HEADER)');
                        return false;
                    }
                    if (!curl_exec($ch)) {
                        trigger_error(curl_error($ch));
                        $error_msg   = curl_error($ch);
                        $this->logger->err($exportName . 'not created CURL Request Error:' . $error_msg);
                        // delete locally "tmp" file
                        unlink($file);
                        exit;
                    }

                    curl_close($ch);
                    fclose($fp);

                    $dateEnd = date("d.m.Y - H:i:s", time());
                    $this->logger->info('Start ' . $exportName . ' CURL Download from Shopware: ' . $dateStart);
                    $this->logger->info('End ' . $exportName . ' CURL Download from Shopware  : ' . $dateEnd);

                    return $file;
                } else {
                    $this->logger->err('CURL Error: fail f_open()');
                    return false;
                }
            } else {
                $this->logger->err('CURL Error: fail curl_init()');
                return false;
            }
        }
    }

    /**
     * check if the CURL functions exists
     * @return bool
     */
    private function checkCurlBasicFunctions(): bool
    {
        if (
            !function_exists("curl_init") &&
            !function_exists("curl_setopt") &&
            !function_exists("curl_exec") &&
            !function_exists("curl_close")
        ) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @return resource $stream
     * @throws Exception
     */

    private function getStream()
    {
        $logFileName = $this->config->get('logFile');

        if (!file_exists($logFileName)) {
            touch($logFileName);
        }
        try {
            return @fopen($logFileName, 'a', false);
        } catch (Exception $e) {
            throw new Exception('failed to open stream for Logging ' . $e->getMessage());
        }
    }


    /**
     * get the config for environment
     *
     * @return Config
     *
     */
    private function getConfigForEnv(): ?Config
    {
        if ($_ENV['APP_ENV'] === 'dev') {
            return new Config(include 'config-development.php');
        } elseif ($_ENV['APP_URL'] === 'https://sw1.it-market.com/' ) {
            return new Config(include 'config-staging.php');
        } elseif ($_ENV['APP_ENV'] === 'prod') {
            return new Config(include 'config-production.php');
        } else {
            $this->logger->crit('no Config File for :' . $_ENV['APP_ENV'] . 'found');
            return null;
        }
    }

    /**
     * check if the created File: FileExtension and in valid Format
     * xml / csv
     * @param string $exportFile
     * @return bool
     * @throws PhpCsvValidatorException
     */
    public function checkExportFile(string $exportFile): bool
    {
        $acceptedExtensions = array('xml', 'csv');
        $pathInfo = pathinfo($exportFile);

        $fileExtension = strtolower($pathInfo['extension']);
        $filePath = $pathInfo['dirname'] . '/' . $pathInfo['basename'];

        if (in_array($fileExtension, $acceptedExtensions)) {
            if ($fileExtension == 'xml') {
                // disable forwarding those load() errors to PHP
                libxml_use_internal_errors(true);
                // initiate the DOMDocument and attempt to load the XML file
                $dom = new \DOMDocument;
                $dom->load($filePath);

                $isvalidXML = $dom->getElementsByTagName('item')->length > 0;

                if ($isvalidXML) {
                    $this->logger->info('XML scheme is valid ');
                    return $isvalidXML;
                }
                return false;
            }
            if ($fileExtension == 'csv') {
                // check the type of the file
                $validator  = new CsvValidate();
                $validator->loadSchemeFromFile($this->config->schemeFile);
                $isvalidCSV = $validator->isValidFile($exportFile);

                if ($isvalidCSV) {
                    $this->logger->info('CSV is valid ');
                    return $isvalidCSV;
                }
                return false;
            }
        }
        return false;
    }

    /**
     * @param Zend\Config\Config $config
     * @param string $ftpFolder
     * @param string $file
     * @param string $filename
     * @return void|null
     * @throws Exception
     */
    private function copyToFtpServer(Config $config, string $ftpFolder, string $file, string $filename)
    {
        $dateStart = date("d.m.Y - H:i:s", time());
        $conn      = ftp_ssl_connect($config->get('server'));

        if ($conn) {
            if (ftp_login($conn, $config->get('user'), $config->get('pw'))) {
                $this->logger->info('FTP Login success');
                ftp_pasv($conn, true);

                // if the folder exits on the FTP Server so change into it, otherwise
                // create the folder
                if ($ftpFolder !== '') {
                    if (ftp_nlist($conn, $ftpFolder)) {
                        //change dir
                        ftp_chdir($conn, $ftpFolder);
                    } else {
                        // create dir
                        ftp_mkdir($conn, $ftpFolder);
                        //change dir
                        ftp_chdir($conn, $ftpFolder);
                    }
                }

                // upload the export file to the FTP Server in the createdFolder
                /**
                 *  $conn = FTP Connection
                 *  $filename = remote Filename
                 *  $file = local File
                 *  mode = Transfer-Modus - entweder FTP_ASCII oder FTP_BINARY
                 *
                 **/
                if (ftp_put($conn, $filename, $file, FTP_BINARY)) {
                    $this->logger->info('Start FTP Transfer: ' . $dateStart);
                    $this->logger->info('now copy the ' . $file . ' to the FTP Server');
                    $this->logger->info('successfully uploaded ' . $file . ' to the FTP Server');
                    echo 'successfully uploaded ' . $file . ' to the FTP Server - FTP close';
                    ftp_close($conn);
                    $dateEnd = date("d.m.Y - H:i:s", time());
                    $this->logger->info('Ende FTP Transfer: ' . $dateEnd);
                    // delete locally "tmp" file
                    //unlink($file);
                    exit;
                } else {
                    $this->logger->err('There was a problem while uploading '  . $file);
                    exit;
                }
            }
        } else {
            $this->logger->err('could not connect to ' . $config->get('server'));
            return null;
        }
        return null;
    }
}
