<?php

/**
 * Class to connect to a sftp server an upload a file
 *
 */
class SFTPConnection
{
    /**
     * @var resource
     */
    private $connection;

    /**
     * @var $sftp
     */
    private $sftp;

    /**
     * __construct
     *
     * @param string $host
     * @param int $port
     *
     * @throws Exception
     */
    public function __construct(string $host, int $port = 22)
    {
        $this->connection = @ssh2_connect($host, $port);
        if (!$this->connection)
            throw new Exception("Could not connect to $host on port $port.");
    }


    /**
     * SFTP Login
     *
     * @param string $username
     * @param string $password
     *
     * @return void
     *
     * @throws Exception
     */
    public function login(string $username, string $password)
    {
        if (!@ssh2_auth_password($this->connection, $username, $password))
            throw new Exception("Could not authenticate with username $username " .
                "and password $password.");

        $this->sftp = @ssh2_sftp($this->connection);
        if (!$this->sftp)
            throw new Exception("Could not initialize SFTP subsystem.");
    }


    /**
     * uploads the export File per scp to the SFTP Server
     *
     *
     * @param string $local_file
     * @param string $remote_file
     *
     * @return void
     * @throws Exception
     */
    public function uploadFile(string $local_file, string $remote_file)
    {
        if ($_ENV['APP_ENV'] !== 'prod'){
            $testPath = '/home/platformexports/test_sftp_exporte/';
        }

        $dateStart = date("d.m.Y - H:i:s", time());
        $sftp = $this->sftp;
        $stream = @fopen("ssh2.sftp://$sftp/$remote_file", 'w');

        if (! $stream)
            throw new Exception("Could not open file: $remote_file");

        $data_to_send = @file_get_contents($local_file);
        if ($data_to_send === false)
            throw new Exception("Could not open local file: $local_file.");

        if (@fwrite($stream, $data_to_send) === false) {
            throw new Exception("Could not send data from file: $local_file.");
        } else {
            $dateEnd = date("d.m.Y - H:i:s", time());
            var_dump('success --- Start: ' . $dateStart . ' End: ' . $dateEnd);
        }

        @fclose($stream);

    }
}
