<?php
$dateTime  = date('d-m-Y-G-i');
$date      = date('d-m-Y');

return array(
    'schemeFile'  =>  'tests/files/scheme1.json',
    'logFile'     => 'logs/' . $date . '-' . 'cron.log',

    // http requests da nicht zertifzierte Zertifikate nicht unterstützt werden
    //FTP Export Idealo
    'idealo' => array(
        'localFile'  => 'export/' . $dateTime . '_' . 'idealo.csv', //docker
        'filename'   => 'idealo.csv',
        'isSFTP'     => false,
        'url'        => 'https://shop:Z+h8c(em-G<b-wwx@sw1.it-market.com/backend/export/index/idealoexport.csv?feedID=18&hash=9e84151d6b5b545dd323cdbd27ba4020',
        'ftp'        => array(
            'server' => 'alfa3047.alfahosting-server.de',
            'user'   => 'web28986784f6',
            'pw'     => 'J4xNSXLh',
            'folder' => 'TEST_idealo'
        )
    ),

    //FTP Export BilligerDE
    'billigerDE' => array(
        'localFile'  => '/var/www/html/custom/static-plugins/cron/export/' . $dateTime . '_' . 'billiger.csv', //docker
        'filename'   => 'billiger.csv',
        'isSFTP'     => false,
        'url'        => 'http://sw2.it-market.com/de/store-api/product-export/SWPESE0ZBVJGWFL4WLFRMZKXSG/billiger.csv',
        'ftp'        => array(
            'server' => 'alfa3047.alfahosting-server.de',
            'user'   => 'web28986784f10',
            'pw'     => 'c2FaTDdq',
            'folder' => 'TEST_billiger'
        )
    ),

    //FTP Export Geizhals
    'geizhals' => array(
        'localFile'  => '/var/www/html/custom/static-plugins/cron/export/' . $dateTime . '_' . 'geizhals.csv', //docker
        'filename'   => 'geizhals.csv',
        'isSFTP'     => false,
        'url'        => 'http://sw2.it-market.com/de/store-api/product-export/SWPESJY5UGRXAUJBCNDYV1VWMW/geizhals.csv',
        'ftp'        => array(
            'server' => 'alfa3047.alfahosting-server.de',
            'user'   => 'web28986784f2',
            'pw'     => 'RSmweppx',
            'folder' => 'TEST_geizhals'
        )
    ),

    //FTP Export guenstigerDE
    'guenstigerDE' => array(
        'localFile'  => '/var/www/html/custom/static-plugins/cron/export/' . $dateTime . '_' . 'guenstiger.csv', //docker
        'filename'   => 'guenstiger.csv',
        'isSFTP'     => false,
        'url'        => 'http://sw2.it-market.com/de/store-api/product-export/SWPECUK3S1ZGYUX2WU5ZVWLCOA/guenstiger.csv',
        'ftp'        => array(
            'server' => 'alfa3047.alfahosting-server.de',
            'user'   => 'web28986784f7',
            'pw'     => 'Z0qgLama',
            'folder' => 'TEST_guenstiger'
        )
    ),

    //FTP Export schottenland
    'schottenland' => array(
        'localFile'  => '/var/www/html/custom/static-plugins/cron/export/' . $dateTime . '_' . 'schottenland.csv', //docker
        'filename'   => 'schottenland.csv',
        'isSFTP'     => false,
        'url'        => 'http://sw2.it-market.com/de/store-api/product-export/SWPEWKMWRKDKBW0WNDBXT3JWVW/schottenland.csv',
        'ftp'        => array(
            'server' => 'alfa3047.alfahosting-server.de',
            'user'   => 'web28986784f9',
            'pw'     => 'zxRI2fre',
            'folder' => 'TEST_schottenland'
        )
    ),
    //SFTP Export GoogleDE und Datei für BING
    'googleDEXML' => array(
        'localFile'  => '/var/www/html/custom/static-plugins/cron/export/' . $date . '_' . 'googlemerchant_de.xml', //docker
        'filename'   => 'googlemerchant_de.xml',
        'isSFTP'     => true,
        'url'        => 'http://sw2.it-market.com/de/store-api/product-export/SWPEBZLUSU9MQTLNNEXSQKTAOA1/googlemerchant_de.xml',
        'ftp'        => array(
            //zum SFTP Testexport Server CronJobs missbraucht :P
            'server' => '172.22.54.35',
            'user'   => 'platformexports',
            'pw'     => 'Jobs2021!',
            'port'   => 22,
            'folder' => ''
        )
    ),

    //SFTP Export GoogleEN
    'googleENXML' => array(
        'localFile'  => '/var/www/html/custom/static-plugins/cron/export/' . $dateTime . '_' . 'googlemerchant_eu_en.xml', //docker
        'filename'   => 'googlemerchant_eu_en.xml',
        'isSFTP'     => true,
        'url'        => 'http://sw2.it-market.com/en/store-api/product-export/SWPEEDJBSLO5DNNNWLJXSNVMWG/googlemerchant_eu_en.xml',
        'ftp'        => array(
            //zum SFTP Testexport Server CronJobs missbraucht :P
            'server' => '172.22.54.35',
            'user'   => 'platformexports',
            'pw'     => 'Jobs2021!',
            'port'   => 22,
            'folder' => ''
        )
    ),

    //SFTP Export GoogleUS
    'googleUSXML' => array(
        'localFile'  => '/var/www/html/custom/static-plugins/cron/export/' . $dateTime . '_' . 'googlemerchant_usa_en.xml', //docker
        'filename'   => 'googlemerchant_usa_en.xml',
        'isSFTP'     => true,
        'url'        => 'http://sw2.it-market.com/en/store-api/product-export/SWPEWVMXUELTVZZQC2DMAZRRTQ/googlemerchant_usa_en.xml',
        'ftp'        => array(
            //zum SFTP Testexport Server CronJobs missbraucht :P
            'server' => '172.22.54.35',
            'user'   => 'platformexports',
            'pw'     => 'Jobs2021!',
            'port'   => 22,
            'folder' => ''
        )
    ),
    //SFTP Export GoogleDE2 und bingXML
    //vorhandene GoogleDE Datei nutzen und diese als bingXML bzw GoogleDE2 exportieren
    'bingXML' => array(
        'localFile'  => '/var/www/html/custom/static-plugins/cron/export/' . $date . '_' . 'googlemerchant_de.xml',
        'filename'   => 'bing_ads_test.xml',
        'isSFTP'     => true,
        'ftp' => array(

            //zum SFTP Testexport Server CronJobs missbraucht :P
            'server' => '172.22.54.35',
            'user'   => 'platformexports',
            'pw'     => 'Jobs2021!',
            'port'   => 22,
            'folder' => ''
        ),
    ),

    'GoogleDE2' => array(
        'localFile'  => '/var/www/html/custom/static-plugins/cron/export/' . $date . '_' . 'googlemerchant_de.xml',
        'filename'   => 'googlemerchant_de2.xml',
        'isSFTP'     => true,
        'ftp' => array(

            //zum SFTP Testexport Server CronJobs missbraucht :P
            'server' => '172.22.54.35',
            'user'   => 'platformexports',
            'pw'     => 'Jobs2021!',
            'port'   => 22,
            'folder' => ''
        ),
    ),

);
