<?php
$dateTime  = date('d-m-Y-G-i');
$date      = date('d-m-Y');

return array(
    'schemeFile'  =>  $_SERVER['HOME'] .  '/sw1.it-market.com/custom/cron/tests/files/scheme1.json',
    'logFile'     =>  $_SERVER['HOME'] .  '/sw1.it-market.com/custom/cron/logs/' . $date . '-' . 'cron.log',

    // http requests da nicht zertifzierte Zertifikate nicht unterstützt werden
    //FTP Export Idealo
    'idealo' => array(
        'localFile'  => $_SERVER['HOME'] . '/sw1.it-market.com/custom/cron/export/' . $dateTime . '_' . 'idealo.csv',
        'filename'   => 'idealo.csv',
        'isSFTP'     => false,
        'url'        => 'https://shop:Z+h8c(em-G<b-wwx@sw1.it-market.com/backend/export/index/idealoexport.csv?feedID=18&hash=9e84151d6b5b545dd323cdbd27ba4020',
        'ftp'        => array(
            'server' => 'alfa3047.alfahosting-server.de',
            'user'   => 'web28986784f6',
            'pw'     => 'J4xNSXLh',
            'folder' => 'TEST_idealo'
        )
    ),

    //FTP Export BilligerDE
    'billigerDE' => array(
        'localFile'  =>  $_SERVER['HOME'] . '/sw1.it-market.com/custom/cron/export/' . $dateTime . '_' . 'billiger.csv', //docker
        'filename'   => 'billiger.csv',
        'isSFTP'     => false,
        'url'        => 'https://shop:Z+h8c(em-G<b-wwx@sw1.it-market.com/backend/export/index/billiger.csv?feedID=3&hash=9ca7fd14bc772898bf01d9904d72c1ea',
        'ftp'        => array(
            'server' => 'alfa3047.alfahosting-server.de',
            'user'   => 'web28986784f10',
            'pw'     => 'c2FaTDdq',
            'folder' => 'TEST_billiger'
        )
    ),

    //FTP Export Geizhals
    'geizhals' => array(
        'localFile'  =>  $_SERVER['HOME'] . '/sw1.it-market.com/custom/cron/export/' . $dateTime . '_' . 'geizhals.csv', //docker
        'filename'   => 'geizhals.csv',
        'isSFTP'     => false,
        'url'        => 'https://shop:Z+h8c(em-G<b-wwx@sw1.it-market.com/backend/export/index/geizhals.csv?feedID=8&hash=0102715b70fa7d60d61c15c8e025824a',
        'ftp'        => array(
            'server' => 'alfa3047.alfahosting-server.de',
            'user'   => 'web28986784f2',
            'pw'     => 'RSmweppx',
            'folder' => 'TEST_geizhals'
        )
    ),

    //FTP Export guenstigerDE
    'guenstigerDE' => array(
        'localFile'  => $_SERVER['HOME'] . '/sw1.it-market.com/custom/cron/export/' . $dateTime . '_' . 'guenstiger.csv', //docker
        'filename'   => 'guenstiger.csv',
        'isSFTP'     => false,
        'url'        => 'https://shop:Z+h8c(em-G<b-wwx@sw1.it-market.com/backend/export/index/guenstiger.csv?feedID=7&hash=5428e68f168eae36c3882b4cf29730bb',
        'ftp'        => array(
            'server' => 'alfa3047.alfahosting-server.de',
            'user'   => 'web28986784f7',
            'pw'     => 'Z0qgLama',
            'folder' => 'TEST_guenstiger'
        )
    ),

    //FTP Export schottenland
    'schottenland' => array(
        'localFile'  => $_SERVER['HOME'] . '/sw1.it-market.com/custom/cron/export/' . $dateTime . '_' . 'schottenland.csv', //docker
        'filename'   => 'schottenland.csv',
        'isSFTP'     => false,
        'url'        => 'https://shop:Z+h8c(em-G<b-wwx@sw1.it-market.com/backend/export/index/schottenland.csv?feedID=6&hash=ad16704bf9e58f1f66f99cca7864e63d',
        'ftp'        => array(
            'server' => 'alfa3047.alfahosting-server.de',
            'user'   => 'web28986784f9',
            'pw'     => 'zxRI2fre',
            'folder' => 'TEST_schottenland'
        )
    ),
    //SFTP Export GoogleDE und Datei für BING
    'googleDEXML' => array(
        'localFile'  => $_SERVER['HOME'] . '/sw1.it-market.com/custom/cron/export/' . $date . '_' . 'googlemerchant_de.xml', //docker
        'filename'   => 'googlemerchant_test_de.xml',
        'isSFTP'     => true,
        'url'        => 'https://shop:Z+h8c(em-G<b-wwx@sw1.it-market.com/backend/export/index/googlemerchant_de.xml?feedID=15&hash=e8eca3b3bbbad77afddb67b8138900e1',
        'ftp'        => array(
            'server' => 'partnerupload.google,com',
            'user'   => 'mc-sftp-10632078',
            'pw'     => '',
            'port'   => 19321,
            'folder' => ''
        )
    ),

    //SFTP Export GoogleEN
    'googleENXML' => array(
        'localFile'  =>  $_SERVER['HOME']  . '/sw1.it-market.com/custom/cron/export/' . $dateTime . '_' . 'googlemerchant_eu_en.xml', //docker
        'filename'   => 'googlemerchant_test_eu_en.xml',
        'isSFTP'     => true,
        'url'        => 'https://shop:Z+h8c(em-G<b-wwx@sw1.it-market.com/backend/export/index/googlemerchant_eu_en.xml?feedID=27&hash=a66426f0adbdfc7997c5d569b7c18f1c',
        'ftp'        => array(
            'server' => 'partnerupload.google,com',
            'user'   => 'mc-sftp-10632078',
            'pw'     => '',
            'port'   => 19321,
            'folder' => ''
        )
    ),

    //SFTP Export GoogleUS
    'googleUSXML' => array(
        'localFile'  => $_SERVER['HOME'] . '/sw1.it-market.com/custom/cron/export/' . $dateTime . '_' . 'googlemerchant_usa_en.xml', //docker
        'filename'   => 'googlemerchant_test_usa_en.xml',
        'isSFTP'     => true,
        'url'        => 'https://shop:Z+h8c(em-G<b-wwx@sw1.it-market.com/backend/export/index/googlemerchant_usa_en.xml?feedID=29&hash=182b3c1888914088112ddb61494d8275',
        'ftp'        => array(
            'server' => 'partnerupload.google,com',
            'user'   => 'mc-sftp-190779115',
            'pw'     => '',
            'port'   => 19321,
            'folder' => ''
        )
    ),
    //SFTP Export GoogleDE2 und bingXML
    //vorhandene GoogleDE Datei nutzen und diese als bingXML bzw GoogleDE2 exportieren
    'bingXML' => array(
        'localFile'  => $_SERVER['HOME'] . '/sw1.it-market.com/custom/cron/export/' . $date . '_' . 'googlemerchant_test_de.xml',
        'filename'   => 'bing_ads_test.xml',
        'isSFTP'     => true,
        'ftp' => array(
            'server' => 'sftp.ads.microsoft.com',
            'user'   => 'itmarket',
            'pw'     => 'Cybertrading2017!',
            'port'   => 19321,
            'folder' => ''
        ),
    ),

    'GoogleDE2' => array(
        'localFile'  => $_SERVER['HOME'] . '/sw1.it-market.com/custom/cron/export/' . $date . '_' . 'googlemerchant_test_de.xml',
        'filename'   => 'googlemerchant_de2.xml',
        'isSFTP'     => true,
        'ftp' => array(
            'server' => 'partnerupload.google,com',
            'user'   => 'mc-sftp-10632078',
            'pw'     => '',
            'port'   => 19321,
            'folder' => ''
        ),
    ),
);
