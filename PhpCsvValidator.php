<?php
//require_once $_SERVER['HOME'] . '/' . $_SERVER['SERVER_NAME'] . '/vendor/autoload.php';
require_once 'vendor/autoload.php';
/**
 * Class PhpCsvValidator
 */
class PhpCsvValidator
{

    /**
     * CSV Schema
     * @var PhpCsvValidatorScheme
     */
    private $scheme = "";

    /**
     * Error Messages
     * @var array
     */
    private $errorMessages = array();

    /**
     * PhpCsvValidator constructor.
     * @param PhpCsvValidatorScheme|null $scheme
     *
     * @throws PhpCsvValidatorException
     */
    function __construct(PhpCsvValidatorScheme $scheme = null)
    {
        if (!is_null($scheme)) {
            if (!$scheme instanceof PhpCsvValidatorScheme) {
                throw new PhpCsvValidatorException("Invalid Scheme!");
            }

            $this->setScheme($scheme);
        }
    }

    /**
     * @param string $row
     * @return bool
     */
    public function isValidRow(string $row): bool
    {
        if(preg_match($this->getScheme()->regex, $row)) {

            return true;
        }

        return false;
    }

    /**
     * Validate CSV File against Scheme
     *
     * @param string $file
     * @return bool
     *
     * @throws PhpCsvValidatorException
     */
    public function isValidFile(string $file): bool
    {
        if (!is_readable($file)) {
            throw new PhpCsvValidatorException("Could not Read CSV File!");
        }

        $rows = file($file);
        foreach ($rows as $lineno => $row) {

            if ($this->scheme->skipFirstLine==1 && $lineno === 0) {
                continue;
            }

            //check only the first line (lineno === 0)
            if ($this->scheme->checkOnlyHeadline==1 && $lineno === 0) {
                if (!$this->isValidRow($row))
                    return false;
                break;
            }

            if (!$this->isValidRow($row))
                return false;
        }
        return true;
    }

    /**
     * @param string $file
     * @return PhpCsvValidator
     *
     * @throws PhpCsvValidatorException
     */
    public function loadSchemeFromFile(string $file): PhpCsvValidator
    {
        if (!is_readable($file)) {
            throw new PhpCsvValidatorException("Could not Read Scheme File!");
        }

        $this->setScheme(new PhpCsvValidatorScheme(file_get_contents($file)));
        return $this;
    }

    /**
     * @return PhpCsvValidatorScheme
     */
    public function getScheme()
    {
        return $this->scheme;
    }

    /**
     * @param PhpCsvValidatorScheme $scheme
     * @return PhpCsvValidator
     */
    public function setScheme(PhpCsvValidatorScheme $scheme): PhpCsvValidator
    {
        $this->scheme = $scheme;

        return $this;
    }

    /**
     * @return array
     */
    public function getErrorMessages(): array
    {
        return $this->errorMessages;
    }

    /**
     * @param string $message
     * @return PhpCsvValidator
     */
    public function setErrorMessages(string $message): PhpCsvValidator
    {
        $this->errorMessages[] = $message;

        return $this;
    }
}

/**
 * Class PhpCsvValidatorException
 */
class PhpCsvValidatorException extends Exception
{
    /**
     * PhpCsvValidatorException constructor.
     * @param string $message
     * @param int $code
     * @param Exception|null $previous
     */
    public function __construct($message, $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return string
     */
    public function getExceptionMessage(): string
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}

/**
 * Class PhpCsvValidatorScheme
 */
class PhpCsvValidatorScheme
{

    /**
     * @var string
     */
    public $label = "";

    /**
     * @var string
     */
    public $regex = "";

    /**
     * @var int
     */
    public $skipFirstLine = 0;

    /**
     * @var int
     */
    public $checkOnlyHeadline = 0;

    /**
     * PhpCsvValidatorScheme constructor.
     * @param string|null $json
     */
    public function __construct(string $json = null)
    {
        if (!is_null($json)) {
            $this->set(json_decode($json, true));
        }
    }

    /**
     * @param $data
     */
    public function set($data)
    {
        foreach ($data AS $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }
    }
}
